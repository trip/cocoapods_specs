
Pod::Spec.new do |s|
  s.name         = "Feedfm"
  s.version      = "0.0.8"
  s.summary      = "The Feed Media SDK for iOS allows you to play DMCA compliant radio within your iOS apps."
  s.description  = <<-DESC
			The Feed Media SDK for iOS allows you to play DMCA compliant radio within your iOS apps. You can read more about the Feed Media API at http://feed.fm/. The primary object you will use to access the Feed Media API is the FMAudioPlayer singleton, which uses AVFoundation for audio playback.
                   DESC
  s.homepage     = "http://developer.feed.fm/documentation#ios"
  s.license      = 'MIT'
  s.author       = { "James Anthony" => "james@fuzz.com" }
  s.platform     = :ios, '6.0'
  s.source       = { :git => "https://trip@bitbucket.org/trip/feedfm-ios-cocoapods.git", :tag => "0.0.8" }
  s.source_files  = 'FeedMediaSDK/*.{h,m}', 'FeedMediaSDK/**/*.{h.m}'
#  s.public_header_files = 'FeedMediaSDK/*.h'
  s.frameworks = 'CoreMedia', 'AVFoundation', 'SystemConfiguration'
  s.requires_arc = true
  s.xcconfig = { "GCC_PREPROCESSOR_DEFINITIONS" => '$(inherited) NSLOGGER_WAS_HERE=1' }
end
