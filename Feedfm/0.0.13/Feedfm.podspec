
Pod::Spec.new do |s|
  s.name         = "Feedfm"
  s.version      = "0.0.13"
  s.summary      = "The Feed Media SDK for iOS allows you to play DMCA compliant radio within your iOS apps."
  s.description  = <<-DESC
			The Feed Media SDK for iOS allows you to play DMCA compliant radio within your iOS apps. You can read more about the Feed Media API at http://feed.fm/. The primary object you will use to access the Feed Media API is the FMAudioPlayer singleton, which uses AVFoundation for audio playback.
                   DESC
  s.homepage     = "http://developer.feed.fm/documentation#ios"
  s.license      = 'MIT'
  s.author       = { "James Anthony" => "james@fuzz.com" }
  s.platform     = :ios, '5.1'
  s.ios.deployment_target = '5.1'
  s.source       = { :git => "https://trip@bitbucket.org/trip/feedfm-ios-cocoapods.git", :tag => s.version.to_s }
  s.source_files = 'Classes', 'Classes/**/*.{h,m}' 
  #example s.prefix_header_file = 'src/PlaynomicsSDK/PlaynomicsSDK-Prefix.pch'
  #example   s.public_header_files = 'src/PlaynomicsSDK/Playnomics.h', 'src/PlaynomicsSDK/PNLogger.h'
  #s.public_header_files = 'src/PlaynomicsSDK/Playnomics.h', 'src/PlaynomicsSDK/PNLogger.h'
  #s.dependency     'JSONKit', '1.5pre' 
  s.frameworks = 'CoreMedia', 'AVFoundation', 'SystemConfiguration'
  #example s.weak_frameworks = 'AdSupport'
  s.requires_arc = true
  s.xcconfig = { "GCC_PREPROCESSOR_DEFINITIONS" => '$(inherited) FEED_WAS_HERE=1' }
end
